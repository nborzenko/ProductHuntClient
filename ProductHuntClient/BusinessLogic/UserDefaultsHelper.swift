//
//  UserDefaultsHelper.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 30.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import Foundation

class UserDefaultsHelper {
    
    enum StringKeys: String {
        case category = "CategoryKey"
    }
    
    enum IntKeys: String {
        case lastProductId = "LastProductIdKey"
    }
    
    static func set(_ value: String, for key: StringKeys) {
        UserDefaults.standard.set(value, forKey: key.rawValue)
    }
    
    static func getValue(for key: StringKeys) -> String? {
        return UserDefaults.standard.string(forKey: key.rawValue)
    }
    
    static func set(_ value: Int, for key: IntKeys) {
        UserDefaults.standard.set(value, forKey: key.rawValue)
    }
    
    static func getValue(for key: IntKeys) -> Int {
        return UserDefaults.standard.integer(forKey: key.rawValue)
    }
}
