//
//  NotificationsManager.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 30.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import Foundation
import UserNotifications

class NotificationsManager {
    
    static let identifier = "ProductHuntNotification"
    
    static func createNotification(for items: [Product]) {
        let center = UNUserNotificationCenter.current()

        let content = UNMutableNotificationContent()
        content.sound = UNNotificationSound.default()
        
        if items.count > 1 {
            content.title = "New items"
            content.body = "\(items.count) items added"
        } else {
            content.title = "New item"
            content.body = "\(items[0].title!)\n\(items[0].tagline!)\n\(items[0].votes!) votes"
        }
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        center.add(request, withCompletionHandler: nil)
    }
}
