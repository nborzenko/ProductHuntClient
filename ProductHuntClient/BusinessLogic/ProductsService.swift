//
//  ProductsService.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 29.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import Foundation

class ProductsService {
    
    private let networkManager = NetworkManager()
    private let jsonParser = JsonParser()
    private let queue = OperationQueue()
    
    private let allEndPoint = "/all"
    private let postsEndPoint = "/posts"
    private let categoriesEndPoint = "/categories"
    
    func getAllCategories(completion: @escaping (Result<[Category]>) -> Void) {
        queue.addOperation { [unowned self] in
            var result: Result<[Category]>
            
            let request = RequestParameters(endPoint: self.categoriesEndPoint, method: .get)
            let response = self.networkManager.makeRequest(parameters: request)
            
            var body: CategoriesResponse? = nil
            if let bodyString = response.body {
                body = self.jsonParser.convert(string: bodyString)
            }
            
            if let body = body, response.status >= 200 && response.status < 300 {
                result = Result<[Category]>.success(body.categoriesList)
            } else {
                result = Result<[Category]>.failure(response.error ?? "Parsing error")
            }
            
            OperationQueue.main.addOperation {
                completion(result)
            }
        }
    }
    
    func getAllProducts(categoryId: String, completion: @escaping (Result<[Product]>) -> Void) {
        queue.addOperation { [unowned self] in
            var result: Result<[Product]>
            
            let endPoint = String(format: "%@/%@%@", self.categoriesEndPoint, categoryId, self.postsEndPoint)
            let request = RequestParameters(endPoint: endPoint, method: .get)
            let response = self.networkManager.makeRequest(parameters: request)
            
            var body: ProductsResponse? = nil
            if let bodyString = response.body {
                body = self.jsonParser.convert(string: bodyString)
            }
            
            if let body = body, response.status >= 200 && response.status < 300 {
                result = Result<[Product]>.success(body.productsList)
            } else {
                result = Result<[Product]>.failure(response.error ?? "Parsing error")
            }
            
            OperationQueue.main.addOperation {
                completion(result)
            }
        }
    }
}
