//
//  AppDelegate.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 28.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let service = ProductsService()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in }
        
        window = UIWindow(frame: UIScreen.main.bounds)

        let storyboard = UIStoryboard.init(name: "Products", bundle: nil)
        let controller = storyboard.instantiateInitialViewController()
        
        window?.rootViewController = controller
        window?.makeKeyAndVisible()
        
        return true
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        guard let categoryId = UserDefaultsHelper.getValue(for: .category) else {
            completionHandler(.noData)
            return
        }
        
        service.getAllProducts(categoryId: categoryId) { result in
            switch result {
            case .success(var items):
                guard items.count > 0 else {
                    completionHandler(.noData)
                    return
                }
                
                let productId = UserDefaultsHelper.getValue(for: .lastProductId)
                UserDefaultsHelper.set(items[0].id!, for: .lastProductId)
                
                if let index = items.index(where: { $0.id == productId }) {
                    items.removeSubrange(index..<items.endIndex)
                }
                
                guard items.count > 0 else {
                    completionHandler(.noData)
                    return
                }
                
                NotificationsManager.createNotification(for: items)
                completionHandler(.newData)
            case .failure(_):
                completionHandler(.failed)
            }
        }
    }
}

