//
//  ProductDetailViewController.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 28.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import UIKit
import AlamofireImage

class ProductDetailViewController: UIViewController {

    @IBOutlet weak var screenshotImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var taglineLabel: UILabel!
    @IBOutlet weak var votesLabel: UILabel!
    
    var model: ProductViewModel?
    
    private let segueId = "ShowWebPage"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let model = model {
            navigationItem.title = model.title
            titleLabel.text = model.title
            taglineLabel.text = model.tagline
            votesLabel.text = "\(model.votes) votes"
            screenshotImageView.af_setImage(withURL: model.screenshot!)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueId {
            let destination = segue.destination as! WebViewController
            destination.url = model?.site
        }
    }
}
