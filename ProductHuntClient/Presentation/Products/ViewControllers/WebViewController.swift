//
//  WebViewController.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 28.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var safariButton: UIBarButtonItem!
    
    var url: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = url {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
            safariButton.isEnabled = true
        }
    }

    @IBAction func openInSafari(_ sender: Any) {
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
}
