//
//  ProductsViewController.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 28.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import UIKit
import AlamofireImage

class ProductsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var categoryButton: UIButton!
    
    fileprivate var categories = [Category]()
    fileprivate var currentCategory = 0
    fileprivate var data = [ProductViewModel]()
    
    fileprivate let cellReuseId = String(describing: ProductTableViewCell.self)
    fileprivate let segueId = "ShowProductDetail"
    
    private let service = ProductsService()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        let nib = UINib(nibName: String(describing: ProductTableViewCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellReuseId)
        
        let refresh = UIRefreshControl()
        refresh.backgroundColor = Colors.orange
        refresh.tintColor = Colors.white
        refresh.addTarget(self, action: #selector(beginRefresh(sender:)), for: .valueChanged)
        tableView.refreshControl = refresh
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 96
        tableView.tableFooterView = UIView()
        
        let savedCategory = UserDefaultsHelper.getValue(for: .category) ?? "tech"
        loadCategories(categoryId: savedCategory)
        loadProducts(categoryId: savedCategory)
    }
    
    func loadProducts(categoryId: String, completion: (() -> Void)? = nil) {
        service.getAllProducts(categoryId: categoryId) { [unowned self] result in
            switch result {
            case .success(let items):
                self.data = items.map {
                    ProductViewModel(title: $0.title!,
                                     tagline: $0.tagline!,
                                     votes: $0.votes!,
                                     thumbnail: URL(string: $0.thumbnail!.imageUrl!),
                                     screenshot: URL(string: $0.screenshot!.imageUrl!),
                                     site: URL(string: $0.webPage!)
                    )
                }
                self.tableView.reloadData()
                let id = items.count > 0 ? items[0].id! : 0
                UserDefaultsHelper.set(id, for: .lastProductId)
            case .failure(_):
                break;
            }
            completion?()
        }
    }
    
    func loadCategories(categoryId: String) {
        service.getAllCategories { [unowned self] result in
            switch result {
            case .success(let items):
                self.categories = items
                self.currentCategory = self.categories.index { $0.categoryId! == categoryId } ?? 0
                self.categoryButton.setTitle(self.categories[self.currentCategory].name, for: .normal)
            case .failure(_):
                return
            }
        }
    }
    
    @IBAction func changeCategory(_ sender: Any) {
        let controller = SelectionViewController()
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        
        controller.delegate = self
        controller.data = categories.map { $0.name! }
        controller.selectedIndex = currentCategory
        
        present(controller, animated: true, completion: nil)
    }
    
    @objc private func beginRefresh(sender: UIRefreshControl) {
        if currentCategory < categories.count {
            loadProducts(categoryId: categories[currentCategory].categoryId!) { [unowned self] in
                self.tableView.refreshControl?.endRefreshing()
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueId {
            let destination = segue.destination as! ProductDetailViewController
            let indexPath = sender as! IndexPath
            destination.model = data[indexPath.row]
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}

extension ProductsViewController: SelectionDelegate {
    func selectionFinished(_ index: Int) {
        currentCategory = index
        categoryButton.setTitle(categories[index].name, for: .normal)
        UserDefaultsHelper.set(categories[index].categoryId!, for: .category)
        loadProducts(categoryId: categories[index].categoryId!)
    }
}

extension ProductsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: segueId, sender: indexPath)
    }
}

extension ProductsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? data.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId, for: indexPath) as! ProductTableViewCell
        cell.thumbnailImageView.af_cancelImageRequest()
        
        let model = data[indexPath.row]
        cell.fill(with: model)
        cell.thumbnailImageView.af_setImage(withURL: model.thumbnail!, placeholderImage: UIImage(named: "Placeholder"))
        return cell
    }
    
}
