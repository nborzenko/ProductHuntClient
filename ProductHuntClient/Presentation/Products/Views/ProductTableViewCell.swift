//
//  ProductTableViewCell.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 28.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var taglineLabel: UILabel!
    @IBOutlet weak var votesLabel: UILabel!
    
    func fill(with model: ProductViewModel) {
        titleLabel.text = model.title
        taglineLabel.text = model.tagline
        votesLabel.text = "\(model.votes) votes"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        let selectedView = UIView()
        selectedView.backgroundColor = Colors.orangeHighlight
        selectedBackgroundView = selectedView
    }
}
