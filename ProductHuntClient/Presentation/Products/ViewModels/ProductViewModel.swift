//
//  ProductViewModel.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 28.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import Foundation

class ProductViewModel {
    var thumbnail: URL?
    var screenshot: URL?
    var site: URL?
    var title: String
    var tagline: String
    var votes: Int
    
    init(title: String, tagline: String, votes: Int,
         thumbnail: URL? = nil, screenshot: URL? = nil, site: URL? = nil) {
        self.title = title
        self.tagline = tagline
        self.votes = votes
        self.thumbnail = thumbnail
        self.screenshot = screenshot
        self.site = site
    }
}
