//
//  SelectionViewController.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 28.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import UIKit

class SelectionViewController: UIViewController {
    
    weak var delegate: SelectionDelegate?
    var data: [String]?
    var selectedIndex = 0

    @IBOutlet weak var pickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let recognizer = UITapGestureRecognizer()
        recognizer.addTarget(self, action: #selector(closeSelection))
        view.addGestureRecognizer(recognizer)
        
        pickerView.delegate = self
        pickerView.dataSource = self
        if let data = data, selectedIndex < data.count {
            pickerView.selectRow(selectedIndex, inComponent: 0, animated: false)
        }
    }

    @IBAction func selectItem(_ sender: Any) {
        delegate?.selectionFinished(selectedIndex)
        closeSelection()
    }
    
    @IBAction func cancel(_ sender: Any) {
        closeSelection()
    }
    
    func closeSelection() {
        dismiss(animated: true, completion: nil)
    }
}

extension SelectionViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data?[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedIndex = row
    }
    
}

extension SelectionViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return component == 0 && data != nil ? data!.count : 0
    }
}
