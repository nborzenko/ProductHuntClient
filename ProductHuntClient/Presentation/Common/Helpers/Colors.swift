//
//  Colors.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 28.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import UIKit

class Colors {
    static let orange = UIColor(red: 227.0 / 255.0, green: 107.0 / 255.0, blue: 61.0 / 255.0, alpha: 1.0)
    static let orangeHighlight = UIColor(red: 227.0 / 255.0, green: 107.0 / 255.0, blue: 61.0 / 255.0, alpha: 0.2)
    static let white = UIColor.white
}
