//
//  RequestResult.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 29.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import Foundation

class RequestResult {
    var status: Int = -1
    var body: String?
    var error: String?
}
