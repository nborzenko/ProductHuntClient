//
//  Category.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 29.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import Foundation
import ObjectMapper

class Category {
    var id: Int?
    var categoryId: String?
    var name: String?
    
    required init?(map: Map) { }
}

extension Category: Validatable {
    func mapping(map: Map) {
        id          <- map["id"]
        categoryId  <- map["slug"]
        name        <- map["name"]
    }
    
    func validate() -> Bool {
        return id != nil && categoryId != nil && name != nil
    }
}
