//
//  Product.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 28.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import Foundation
import ObjectMapper

class Product: Mappable {
    var id: Int?
    var title: String?
    var tagline: String?
    var createdAt: Date?
    var votes: Int?

    var webPage: String?
    var thumbnail: Thumbnail?
    var screenshot: Screenshot?
    
    required init?(map: Map) { }
}

extension Product: Validatable {
    func mapping(map: Map) {
        id          <- map["id"]
        title       <- map["name"]
        tagline     <- map["tagline"]
        createdAt   <- (map["created_at"], CustomDateFormatTransform(formatString: "YYYY-MM-dd'T'hh:mm:ss.SSSSxxx"))
        votes       <- map["votes_count"]

        webPage     <- map["redirect_url"]
        thumbnail   <- map["thumbnail"]
        screenshot  <- map["screenshot_url"]
    }
    
    func validate() -> Bool {
        return id != nil && title != nil && tagline != nil && createdAt != nil &&
            votes != nil && webPage != nil && thumbnail != nil && screenshot != nil
    }
}
