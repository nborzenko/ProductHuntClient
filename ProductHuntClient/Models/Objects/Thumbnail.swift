//
//  Thumbnail.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 29.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import Foundation
import ObjectMapper

class Thumbnail {
    var imageUrl: String?
    
    required init?(map: Map) { }
}

extension Thumbnail: Validatable {
    func mapping(map: Map) {
        imageUrl <- map["image_url"]
    }
    
    func validate() -> Bool {
        return imageUrl != nil
    }
}
