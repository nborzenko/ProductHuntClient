//
//  Screenshot.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 29.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import Foundation
import ObjectMapper

class Screenshot {
    var imageUrl: String?
    
    required init?(map: Map) { }
}

extension Screenshot: Validatable {
    func mapping(map: Map) {
        imageUrl <- map["300px"]
    }
    
    func validate() -> Bool {
        return imageUrl != nil
    }
}
