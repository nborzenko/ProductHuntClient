//
//  Result.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 29.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(String)
    
    init(_ value: T) {
        self = .success(value)
    }
    
    init(_ error: String) {
        self = .failure(error)
    }
}
