//
//  ProductsResponse.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 29.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import Foundation
import ObjectMapper

struct ProductsResponse {
    var productsList: [Product]!
}

extension ProductsResponse: Validatable {
    init(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        productsList <- map["posts"]
    }
    
    func validate() -> Bool {
        guard productsList != nil else {
            return false
        }
        for product in productsList {
            if !product.validate() {
                return false
            }
        }
        return true
    }
}
