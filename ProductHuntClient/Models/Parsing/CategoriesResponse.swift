//
//  CategoriesResponse.swift
//  ProductHuntClient
//
//  Created by n-borzenko on 29.04.17.
//  Copyright © 2017 nborzenko. All rights reserved.
//

import Foundation
import ObjectMapper

struct CategoriesResponse {
    var categoriesList: [Category]!
}

extension CategoriesResponse: Validatable {
    init(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        categoriesList <- map["categories"]
    }
    
    func validate() -> Bool {
        guard categoriesList != nil else {
            return false
        }
        for category in categoriesList {
            if !category.validate() {
                return false
            }
        }
        return true
    }
}
